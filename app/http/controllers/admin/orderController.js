const order = require("../../models/order")
const moment = require('moment');
//const Order = require('../../models/order')
const stripe = require('stripe')(process.env.STRIPE_PRIVATE_KEY)
function orderController() {
    return {
        index(req, res) {
           order.find({ status: { $ne: 'completed' } }, null, 
           { sort: { 'createdAt': -1 }})
           .populate('customerId', '-password').exec((err, orders) => {
               if(req.xhr) {
                   return res.json(orders)
               } else {
                return res.render('admin/orders')
               }
           })
        }
    }
}

module.exports = orderController

// const order = require ('../../models/order');

// function orderController(){
//     return{
//         index(req,res){
//             order.find({ status :{ $ne : 'completed'}},null,{sort :{'createdAt': -1}}).populate('customerId','-password').exec((err, orders)=>{
//                 // if(req.xhr){
//                 //     console.log(orders);
//                 //     return res.json(orders)
                    
//                 // }else{
//                     return res.render('admin/orders')
//                 // }
//             })
//         }
//     }
// }
// module.exports = orderController