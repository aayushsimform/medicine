
const Menu = require('../../models/menu')
const multer = require('multer');


var storage = multer.diskStorage({
    destination: function(req,file,cb) {
      cb(null,'public/img')
      
    },
    filename: function(req,file,cb) {
      cb(null,Date.now()+file.originalname)
      
    }
  })
  //var upload = multer({ storage:storage});
  
  
  
  const fileFilter = (req,file,cb) => {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'images/jpg' || file.mimetype ==='images/png'){
         cb(null,true)
    }else{
         cb(null,false)
    }
  }
  var upload = multer({
    storage:storage,
    //  limits:{
    //    fileSize: 100000
    //  },
   // fileFilter:fileFilter
  }).single('')
  


function productController(){
    return{
         async index(req, res) {
            const medicine = await Menu.find()
            //console.log(medicine);
            return res.render('home', { medicine: medicine })
          //console.log(Menu.find());
        // Menu.find().then(function(medicine){
        //     console.log(medicine);
        //    return res.render('home', { medicine: medicine })
            
       // })
      //return res.render('home')
    },
    add(req,res){
         const menu = new Menu({
             name : req.body.name,
              //image : req.file.image,
             price : req.body.price,
             size : req.body.size
         });
         menu.save().then( menu =>{
           
            // Login
            return res.redirect('/admin/product')
           
        }).catch(err => {
            req.flash('error', 'Something went wrong')
                return res.send(err)
         })   
    }
    }
}
module.exports = productController