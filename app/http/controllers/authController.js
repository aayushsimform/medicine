const bcrypt = require('bcrypt');
const passport = require('passport');
const User = require('../../http/models/user');
const nodemailer = require('nodemailer');

function authController( ) {
    const _getRedirectUrl = (req) => {
        return req.user.role === 'admin' ? '/admin/orders' : '/customers/orders'
    }
    return{
        login(req,res){
            res.render('auth/login')
        },
        register(req,res){
            res.render('auth/register')
        },
        // postLogin(req,res){
        //     const { email, password }   = req.body
        //     passport.authenticate('local',(err,user,info)=>{
        //         if(err){
        //             req.flash('err',info.message)
        //             return next(err)
        //         }
        //         if(!user){
        //             req.flash('err',info.message)
        //             return res.redirect('/login')
        //         }
        //         req.logIn(user,(err)=>{
        //             if(err){
        //                 req.flash('error',info.message)
        //                 return next(err)
        //             }
        //             return res.redirect('/')
        //         })
        //     })(req,res,next)
        // },
        postLogin(req, res, next) {
            const { email, password }   = req.body
           // Validate request 
            if(!email || !password) {
                req.flash('error', 'All fields are required')
                return res.redirect('/login')
            }
            passport.authenticate('local', (err, user, info) => {
                if(err) {
                    req.flash('error', info.message )
                    return next(err)
                }
                if(!user) {
                    req.flash('error', info.message )
                    return res.redirect('/login')
                }
                req.logIn(user, (err) => {
                    if(err) {
                        req.flash('error', info.message ) 
                        return next(err)
                    }

                    return res.redirect(_getRedirectUrl(req))
                })
            })(req, res, next)
        },
        logout(req,res){
                   req.logout()
                  return res.redirect('/');
        },
         async postRegister(req, res) {
            const { name, email, password }   = req.body;
            console.log(req.body);
            //validate
            if(!name || !email || !password){
                req.flash('error','All field Required')
                req.flash('name', name)
                req.flash('email', email) 
           
                return res.redirect('/register')
            }   
            // Check if email exists 
         User.exists({ email: email }, (err, result) => {
            if(result) {
               req.flash('error', 'Email already taken')
               req.flash('name', name)
               req.flash('email', email) 
               return res.redirect('/register')
            }
        })
        //hash
        const hashedPassword = await bcrypt.hash(password,10);
        const user = new User({
            name,
            email,
            password: hashedPassword
        })
        user.save().then( user =>{
           
            // Login
            return res.redirect('/')
           
        }).catch(err => {
            req.flash('error', 'Something went wrong')
                return res.redirect('/register')
         })   
    },
    async forget(req,res){
        const { email}   = req.body;
       
        
        // Validate request 
         if(!email) {
             req.flash('error', 'All fields are required')
             return res.redirect('/forget')
         }
         User.exists({ email: email }, async(err, result) => {
            const pwd = await User.findOne({email: email});
        
            let transporter = nodemailer.createTransport({
                service : "gmail",
                auth :{
                    user: "aayushuplift@gmail.com",
                    pass: "Orton@12345"
                },
                tls:{
                    rejectUnauthorized : false,
                }
            })
            let mailOption = {
                from : "aayushuplift@gmail.com",
                to :`${email}`,
                subject: "Your Password",
                html:`${pwd.password}`
            }
            if(result) {
                transporter.sendMail(mailOption, function(err,success) {
                    if(err){
                        console.log(err);
                    }
                    else{
                        req.flash('error', 'Email is sended')
                        return res.redirect('/forget')
                    }
                    
               })
            }
            else{
               req.flash('error', 'Email is not exist')
             return res.redirect('/forget')
            }
        })
    }
    }
}


module.exports = authController