function cartController(){
    return{
        index(req,res){
            res.render('customers/cart')
        },
        update(req,res){
        //     let cart ={
        //         items: {
        //             pizzaId: { item: pizzaObject, qty:0 },
        //         },
        //         totalQty: 0,
        //         totalPrice: 0
                
            
        // }
        if (!req.session.cart) {
            req.session.cart = {
                items: {},
                totalQty: 0,
                totalPrice: 0
            }
        }
        let cart = req.session.cart

        // Check if item does not exist in cart 
        if(!cart.items[req.body._id]) {
            cart.items[req.body._id] = {
                item: req.body,
                qty: 1
            }
            cart.totalQty = cart.totalQty + 1
            cart.totalPrice = cart.totalPrice + req.body.price
        } else {
            cart.items[req.body._id].qty = cart.items[req.body._id].qty + 1
            cart.totalQty = cart.totalQty + 1
            cart.totalPrice =  cart.totalPrice + req.body.price
        }
        return res.json({ totalQty: req.session.cart.totalQty })
            
    },
    delete(req,res){
         const data = req.body;
         let carts = req.session.cart
         console.log('this:',carts.items[data.item._id].qty);
         carts.items[data.item._id].qty = carts.items[data.item._id].qty-1;
         carts.totalQty=carts.totalQty - 1
         carts.totalPrice =  carts.totalPrice - data.item.price
         
         
        if(carts.items[data.item._id].qty === 0){
          delete  carts.items[data.item._id]
        }
        if(carts.totalQty === 0){
            delete req.session.cart
        }
        return res.send({carts})
    },
    add(req,res){
        const data = req.body;
        let carts = req.session.cart
        console.log('this:',carts.items[data.item._id].qty);
        carts.items[data.item._id].qty = carts.items[data.item._id].qty+1;
        carts.totalQty=carts.totalQty+1
        carts.totalPrice =  carts.totalPrice+ data.item.price
        
        
       if(carts.items[data.item._id].qty === 0){
         delete  carts.items[data.item._id]
       }
       if(carts.totalQty === 0){
           delete req.session.cart
       }
       return res.send({carts})
   }
    }
}
module.exports = cartController