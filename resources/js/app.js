import  axios  from 'axios';
import Noty from 'noty';
import { initAdmin } from "./admin";
import moment from 'moment';
import { initstripe } from './stripe';
let addToCart = document.querySelectorAll('.add-to-cart')
let cartCounter = document.querySelector('#cartCounter')
let rmvToCart = document.querySelectorAll('.rmv-to-cart')
let adToCart = document.querySelectorAll('.ad-to-cart')

function updateCart(medicine){
    axios.post('/update-cart', medicine).then(res =>{
        console.log(res);
        cartCounter.innerText = res.data.totalQty;
        new Noty({
            type: 'success',
            timeout: 1000,
            text: 'Item added to cart',
            progressBar: false,
        }).show();
    }).catch(err => {
        new Noty({
            type: 'error',
            timeout: 1000,
            text: 'Something went wrong',
            progressBar: false,
        }).show();
    })
}
 
function removeCart(medicine){
    axios.post('/delete-cart', medicine).then(res =>{
       // console.log(medicine);
        //cartCounter.innerText = res.data.totalQty;
        new Noty({
            type: 'success',
            timeout: 1000,
            text: 'Item removed to cart',
            progressBar: false,
        }).show();
    }).catch(err => {
        new Noty({
            type: 'error',
            timeout: 1000,
            text: 'Something went wrong',
            progressBar: false,
        }).show();
    })
}
function addCart(medicine){
    axios.post('/add-cart', medicine).then(res =>{
  //  console.log(medicine);
        //cartCounter.innerText = res.data.totalQty;
        new Noty({
            type: 'success',
            timeout: 1000,
            text: 'Item added to cart',
            progressBar: false,
        }).show();
    }).catch(err => {
        new Noty({
            type: 'error',
            timeout: 1000,
            text: 'Something went wrong',
            progressBar: false,
        }).show();
    })
}
addToCart.forEach((btn)=>{
    btn.addEventListener('click',(e)=>{
        
        let medicine = JSON.parse(btn.dataset.pizza)
        updateCart(medicine);
    })
})
rmvToCart.forEach((btn)=>{
    btn.addEventListener('click',(e)=>{
        let medicine = JSON.parse(btn.dataset.med)
      //  console.log(medicine);
        removeCart(medicine);
    })
})
adToCart.forEach((btn)=>{
    btn.addEventListener('click',(e)=>{
        let medicine = JSON.parse(btn.dataset.medd)
      //  console.log(medicine);
        addCart(medicine);
    })
})
initstripe();
initAdmin();
let statuses = document.querySelectorAll('.status_line')
let hiddenInput = document.querySelector('#hiddenInput')
let order = hiddenInput ? hiddenInput.value : null
order = JSON.parse(order)
let time = document.createElement('small')
console.log(order);
function updateStatus(order) {
    statuses.forEach((status) => {
        status.classList.remove('step-completed')
        status.classList.remove('current')
    })
    let stepCompleted = true;
    statuses.forEach((status) => {
       let dataProp = status.dataset.status
       if(stepCompleted) {
            status.classList.add('step-completed')
       }
       if(dataProp === order.status) {
            stepCompleted = false
            time.innerText = moment(order.updatedAt).format('hh:mm A')
            status.appendChild(time)
           if(status.nextElementSibling) {
            status.nextElementSibling.classList.add('current')
           }
       }
    })

}

updateStatus(order);


 let socket = io()
 //join
 if(order){
        socket.emit('join',`order_${order._id}`)

 }
 //order_asdfsgdgddfgfd
 socket.on('orderUpdated',(data)=>{
     const updateOrder = { ...order}
     updateOrder.updatedAt  = moment().format()
     updateOrder.status = data.status
     console.log(updateOrder);
 })
