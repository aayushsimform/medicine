import  axios  from 'axios';
import Noty from 'noty';
import { loadStripe } from '@stripe/stripe-js'
import { placeOrder } from './apiService';
export async function initstripe() {
    //ajaxcall 
const stripe = await loadStripe('pk_test_51KTOguSAHIUcnfcpCjy5N0TKwQFgZOsjkFzzXZLmyldF0QIWOfXX5QcJqkcBYVCSlekvFVg01hQgme2feMs9Y5Po00W3x68uGj');
let card = null;
 function mountWidget() {
        const elements = stripe.elements()

    let style = {
        base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
            color: '#aab7c4'
        }
        },
        invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
        }
    };

    card = elements.create('card', { style, hidePostalCode: true })
    card.mount('#card-element')
 }

const paymentType = document.querySelector('#paymentType');
if(!paymentType) {
    return;
}
paymentType.addEventListener('change' , (e)=> {

    if(e.target.value === 'card') {
        // Display Widget
        mountWidget();
       //card = new CardWidget(stripe)
    //    card.mount()
    } else {
       card.destroy()
    }

})


const paymentForm = document.querySelector('#payment-form');
if(paymentForm){
    paymentForm.addEventListener('submit',(e)=>{
        e.preventDefault();
        let formData = new FormData(paymentForm);
        let formObject ={}
        for (let [key,value] of formData.entries()) {
            formObject[key] = value
           // console.log(key);    
        }
        if(!card){
            //Ajax
            placeOrder(formObject);
            return;

        }
        //verify card
        // const token = await card.createToken()
        // formObject.stripeToken = token.id;
        // placeOrder(formObject);
        stripe.createToken(card).then((result)=>{
          console.log(result);
          formObject.stripeToken = result.token.id;
          placeOrder(formObject);
        }).catch(err =>{
            console.log(err);
        })
        // axios.post('/orders',formObject).then((res)=>{
        //        // console.log(res.data);
        //         new Noty({
        //             type: 'success',
        //             timeout: 1000,
        //             text: res.data.message,
        //             progressBar: false,
        //         }).show();
        //     setTimeout(()=>{
        //         window.location.href = '/customers/orders';
                
        //     },2000)
        // }).catch((err)=>{
        //     new Noty({
        //         type: 'success',
        //         timeout: 1000,
        //         text: err.res.err.message,
        //         progressBar: false,
        //     }).show();
        // })
        // console.log(formObject);
    })
    
}
}