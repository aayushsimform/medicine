const authController = require('../app/http/controllers/authController');
const cartController = require('../app/http/controllers/customers/cartController');
const homeController = require('../app/http/controllers/homeController');
const orderController = require('../app/http/controllers/customers/orderController');
const adminOrderController = require('../app/http/controllers/admin/orderController');
const statusController = require('../app/http/controllers/admin/statusController');

const guest = require('../app/middleware/guest')
const auth = require('../app/middleware/auth')
const admin = require('../app/middleware/admin');
const productController = require('../app/http/controllers/admin/productController');

//require('../app/http/controllers/homeController');
//require('../app/http/controllers/authController');

function initRoutes(app) {
   app.get('/', homeController().index)

    app.get('/login',guest,authController().login);
    app.post('/login',authController().postLogin);
    app.get('/admin/product',(req,res)=>{
        res.render('admin/product')
    });
    app.get('/forget',(req,res)=>{
        res.render('auth/forget')
    });
    app.post('/forget',authController().forget);
    app.post('/admin/product',productController().add);
    app.get('/register',guest,authController().register);
    app.post('/register',authController().postRegister);
    app.post('/logout',authController().logout);
    
    app.get('/cart',cartController().index);
    app.post('/update-cart', cartController().update);
    app.post('/delete-cart', cartController().delete);
    app.post('/add-cart', cartController().add);
    app.post('/orders',auth, orderController().store);
    app.get('/customers/orders',auth, orderController().index)
    app.get('/customer/orders/:id', auth, orderController().show)
    //app.get('/customer/orders/:id', auth, orderController().show)
    app.get('/admin/orders',admin,adminOrderController().index)
    app.post('/admin/order/status', admin, statusController().update)


}

module.exports = initRoutes