
require('dotenv').config()
const express = require('express');
const app = express();
const path = require('path');
const session = require('express-session')
const flash = require('express-flash');
const ejs = require('ejs');
const bodyParser = require('body-parser');
const expressLayout = require('express-ejs-layouts');
const MongoDbStore = require('connect-mongo')(session);

const port = process.env.PORT || 5002;
const mongoose = require('mongoose');
const passport = require('passport');
const Emitter = require('events')
require('./app/config/passport')(passport);
app.use(express.static('./public'));
app.set('views',path.join(__dirname,'/resources/views'))
app.set('view engine','ejs')
app.use(expressLayout);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


mongoose.connect(process.env.MONGO_CONNECTION_URL, {  });
const connection = mongoose.connection;
connection.once('open', () => {
    console.log('Database connected...');
}).on('error',err => {
    console.log('Connection failed...')
});

//session

let mongoStore = new MongoDbStore({
  mongooseConnection: connection,
  collection: 'sessions'
});
const eventEmitter = new Emitter()
app.set('eventEmitter',eventEmitter)
app.use(session({
  secret: process.env.COOKIE_SECRET,
  resave: false,
  store: mongoStore,
  saveUninitialized: false,
  cookie: { maxAge: 1000 * 60 * 60 * 24 } // 24 hour
}))

const passportInit = require('./app/config/passport')
 passportInit(passport);
 app.use(passport.initialize())
app.use(passport.session())
app.use(flash());
//connection 

// const url ="mongodb://localhost:27017/medicine";
// mongoose.connect(process.env.MONGO_CONNECTION_URL,(err)=>{
//   if(err){
//   console.log(err);

//   }
//   else{
//     console.log("successfully connected ");
//   }
// })
app.use((req, res, next) => {
  res.locals.session = req.session
  res.locals.user = req.user
  next()
})

require('./routes/web')(app)
const server = app.listen(port,()=>{
    console.log(`server is running ${port}`);
})

//socket

const io = require('socket.io')(server)
io.on('connection',(socket)=>{
      // JOIN 
      socket.on('join',(orderId)=>{
       
            socket.join(orderId)
      })    
})
eventEmitter.on('orderUpdated',(data)=>{
  io.to(`order_${data.id}`).emit('orderUpdated',data)
})

eventEmitter.on('orderPlaced',(data)=>{
  io.to(`adminRoom`).emit('orderPlaced',data)
})

